const gulp = require('gulp');
const dotenv = require('dotenv');
const gulpif = require('gulp-if');
const clean = require('gulp-clean');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const htmlmin = require('gulp-htmlmin');
const uglify = require('gulp-uglify');
const babel = require('gulp-babel');
const data = require('gulp-data');
const browserSync = require('browser-sync').create();
const uglifycss = require('gulp-uglifycss');
const autoprefixer = require('gulp-autoprefixer');
const compress = require('compression');
const nunjucksRender = require('gulp-nunjucks-render');

dotenv.config({
    debug: process.env.DEBUG
});

const isProd = process.env.NODE_ENV === 'production';
const debug = process.env.DEBUG;

const paths = {
    dist: isProd ? process.env.PROD_PATH : process.env.SERVE_PATH,
};

const nunjucksData = {
    baseUrl: process.env.BASEURL,
    isProd: isProd,
    assetsUrl: process.env.BASEURL + '/assets'
};


function handleError(err) {
    log('opps');
    log(err.toString());
    this.emit('end');
}

gulp.task('browserSync', function (done) {
    browserSync.init(null, {
        server: {
            baseDir: "dist",
            middleware: [compress()]
        },
    });
    done();
});

gulp.task('reload', function (done) {
    browserSync.reload();
    log('Bowser reloaded');
    done();
});

gulp.task('sass', function () {
    return gulp.src([
            'app/assets/scss/**/main.scss'
        ])
        .pipe(sass())
        .on('error', handleError)
        .pipe(autoprefixer('last 2 versions'))
        .pipe(gulpif(isProd, uglifycss()))
        .pipe(gulp.dest(paths.dist + '/assets/css/'));
});

gulp.task('css', function () {
    return gulp.src([
            'app/assets/css/bootstrap.min.css',
            'app/assets/css/regular.min.css',
            'app/assets/css/font-awesome.min.css',
            'app/assets/css/dropzone.css',
            'app/assets/css/flatpickr.min.css',
            
        ])
        .pipe(concat('lib.css'))
        .pipe(uglifycss())
        //.pipe(gzip())
        .on('error', handleError)
        .pipe(gulp.dest(paths.dist + '/assets/css/'));
});

gulp.task('js', function () {
    return gulp.src([
            'app/assets/js/jquery-3.5.1.slim.min.js',
            'app/assets/js/bootstrap.bundle.min.js',
            'app/assets/js/dropzone.js',
            'app/assets/js/jquery.validate.js',
            'app/assets/js/flatpickr.js',
            
            
        ])
        .pipe(concat('lib.js'))
        .pipe(uglify())
        .on('error', handleError)
        //.pipe(gzip())
        .pipe(gulp.dest(paths.dist + '/assets/js/'))
        .pipe(browserSync.stream());
});

gulp.task('babel', function () {
    return gulp.src([
            'app/assets/js/main.js'
        ])
        .pipe(concat('main.js'))
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(gulpif(isProd, uglify()))
        .pipe(gulp.dest(paths.dist + '/assets/js/'))
        .pipe(browserSync.stream());
});

gulp.task('images', function () {
    return gulp.src([
            'app/assets/images/**/*',
        ])
        .pipe(gulp.dest(paths.dist + '/assets/images'));
});

gulp.task('fonts', function () {
    return gulp.src([
            'app/assets/fonts/**/*'
        ])
        .pipe(gulp.dest(paths.dist + '/assets/fonts'));
});

gulp.task('html', function () {
    return gulp.src([
            'app/**/*.html'
        ])
        .pipe(gulp.dest(paths.dist));
});

gulp.task('nunjucks', function () {
    nunjucksRender.nunjucks.configure(['app/templates/']);
    // Gets .html and .nunjucks files in pages
    return gulp.src('app/pages/**/*.+(html|njk|nunj|nunjucks)')
        .pipe(data(function () {
            // extend data with nunjucksData
            var data = require('./app/assets/data/data.json');
            data['blog'] = require('./app/assets/data/blogs.json');

            Object.keys(nunjucksData).forEach(function (key) {
                data[key] = nunjucksData[key];
            });
            return data;
        }))
        // Renders template with nunjucks
        .pipe(nunjucksRender({
            // data: nunjucksData,
            path: ['app/templates/'] // String or Array
        }))
        .pipe(gulpif(isProd, htmlmin()))
        // output files in app folder
        .pipe(gulp.dest(paths.dist));
});

gulp.task('watch', function () {
    gulp.watch('app/assets/scss/*.scss', gulp.parallel('sass'));
    gulp.watch('app/assets/scss/utility/*.scss', gulp.parallel('sass'));
    gulp.watch('app/assets/scss/landing/*.scss', gulp.parallel('sass'));
    gulp.watch('app/assets/css/*.css', gulp.parallel('css'));
    gulp.watch([
        'app/assets/js/*.js',
        '!app/assets/js/main.js',
    ], gulp.parallel('js'));
    gulp.watch([
        'app/assets/js/main.js'
    ], gulp.parallel('babel'));
    gulp.watch('app/assets/images/**/*', gulp.parallel('images'));
    gulp.watch('app/assets/fonts/**/*', gulp.parallel('fonts'));
    gulp.watch([
        'app/templates/**/*.+(html|njk|nunj|nunjucks)',
        'app/pages/**/*.+(html|njk|nunj|nunjucks)'
    ], gulp.parallel('nunjucks'));
    gulp.watch([
        'dist/assets/js/*',
        'dist/assets/css/*',
        'dist/assets/images/*',
        'dist/**/*.html',
    ], gulp.series('reload')); //*.+(html|js|css|jpg|png
    log('gulp watching for changes..');
});

gulp.task('message', done => {
    log(`==========> Building for ${process.env.NODE_ENV}`);
    done();
});

gulp.task('clean', function () {
    return gulp.src('dist/*', {
            read: false
        })
        .pipe(gulpif(isProd, clean()));
});

gulp.task('build',
    gulp.series(
        'message',
        'clean',
        gulp.parallel(
            'sass', 'css', 'js', 'babel', 'nunjucks', 'images', 'fonts'
        )));

gulp.task('default',
    gulp.series(
        'build',
        'browserSync', 'watch'
    ));

function log(message) {
    if (debug) {
        let today = new Date().toLocaleTimeString();
        console.log(`[${today}] ${message}`);
    }
    return;
}