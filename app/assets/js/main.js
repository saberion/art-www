$(function () {
	validation();

	validationForms();

	datePicker();
});

function validation() {
	Dropzone.options.myGreatDropzone = {
		// camelized version of the `id`
		maxFiles: 1,
		paramName: "file", // The name that will be used to transfer the file
		maxFilesize: 2, // MB
		accept: function (file, done) {
			if (file.name == "justinbieber.jpg") {
				done("Naha, you don't.");
			} else {
				done();
			}
		},
		maxfilesexceeded: function (file) {
			this.removeAllFiles();
			this.addFile(file);
		},
	};
}

function validationForms() {
	$("#contactForm").validate({
		rules: {
			fullName: {
				required: true,
			},
			dateOfBirth: {
				required: true,
			},
			parentName: {
				required: true,
			},
			email: {
				required: true,
				email: true,
			},
			contactNo: {
				required: true,
				digits: true,
				minlength: 10,
				maxlength: 10,
			},
			"check_confirm[]": {
				required: true,
			},
		},
		messages: {
			fullName: {
				required: "Please provide your full name",
			},
			dateOfBirth: {
				required: "Please provide date of birth",
			},
			parentName: {
				required: "Please provide parent name",
			},
			contactNo: {
				required: "Please provide phone number",
			},
			email: {
				required: "Please provide your email",
			},
			"check_confirm[]": {
				required: "You must check at least 1 box",
			},
		},
	});
}

function datePicker() {
	$(".data-pick").flatpickr({
		altInput: true,
    altFormat: "F j, Y",
    dateFormat: "Y-m-d"
	});
}
